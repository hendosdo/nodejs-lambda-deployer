FROM frolvlad/alpine-python2:latest
MAINTAINER Jamie Graham <jamie.graham@henderson-group.com>

ARG AWS_ACCESS_KEY_ID
ARG AWS_DEFAULT_REGION
ARG AWS_SECRET_ACCESS_KEY

ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
ENV AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY


RUN apk --no-cache update && \
    apk --no-cache add py-pip py-setuptools ca-certificates groff less && \
    pip --no-cache-dir install awscli && \
    rm -rf /var/cache/apk/*

RUN aws configure set aws_access_key_id '$AWS_ACCESS_KEY_ID' --profile claudia && \
	aws configure set aws_secret_access_key '$AWS_SECRET_ACCESS_KEY' --profile claudia && \
	aws configure set default.region eu-west-1 --profile claudia && \
	aws configure set aws_access_key_id '$AWS_ACCESS_KEY_ID' && \
    aws configure set aws_secret_access_key '$AWS_SECRET_ACCESS_KEY' && \
    aws configure set default.region eu-west-1

RUN apk add git

RUN apk add --update nodejs npm && \
	npm i -g claudia claudia-api-builder nodemon npm-run-all

RUN npm install webpack webpack-cli -g

WORKDIR /opt
